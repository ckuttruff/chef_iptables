OPTS = {
  :interface => '-i',
  :source    => '-s',
  :dest      => '-d',
  :protocol  => '-p',
  # Note: if you specify a port, you must specify a protocol :)
  :port      => '--dport',
}

OPTS.each do |meth, flag|
  define_method meth do 
    obj = new_resource
    obj.respond_to?(meth) ? obj.send(meth) : nil
  end
end

def rule_str
  rules = OPTS.map do |meth, flag| 
    val = send(meth)
    val ? "#{flag} #{val}" : nil
  end
  rules.compact.join(' ')
end

def save_rules
  system("iptables-save > /etc/iptables.rules")
end

# ===========================================================================
# action definitions

action :execute do
  system("iptables #{new_resource.command}")
  save_rules()
end

action :allow_input do 
  system("iptables -A INPUT #{rule_str} -j ACCEPT")
  save_rules()
end

# reasonable default stateful firewall ruleset (wipes out all existing rules!!)
action :stateful_ruleset do 
  cmds = [ '-P INPUT ACCEPT', '-P FORWARD ACCEPT', '-P OUTPUT ACCEPT', '-F',
           '-A INPUT -s 127.0.0.0/8 -d 127.0.0.0/8 -i lo  -j ACCEPT', 
           '-A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT',
           # I enable ssh here so we don't get locked out of logging in to 
           #   the system in case of an exception being thrown before ssh rule.
           #   Feel free to comment it out or change the port to whatever ssh
           #   is listening on
           '-A INPUT -p tcp --dport 22 -j ACCEPT',
           '-P INPUT   DROP',
           '-P FORWARD DROP' ]
  cmd_str = cmds.map { |c| "iptables #{c}" }.join(' ; ')
  system(cmd_str)
  save_rules()
end
